# Managed Kubernetes Deployment Pipeline

This project provides an example of a Kubernetes deployment pipeline, managed by a horizontal team. `.gitlab-ci.include.yml` should be included in project-specific pipelines:

```yaml
include: https://gitlab.com/gke-deploy-patterns/pipelines/core/raw/master/.gitlab-ci.include.yml
```

The project-specific pipeline should specify project-specific variables and overrides:

```yaml
include: https://gitlab.com/gke-deploy-patterns/pipelines/core/raw/master/.gitlab-ci.include.yml

variables:
  PROJECT_ID: "12345"
```

The project-specific pipeline should be invoked with a pipeline trigger by the application project, linking back to the JOB_ID, REF, and SHA of the invoking application:

```bash
curl --request POST \
  --form token=$CI_JOB_TOKEN \
  --form ref=master \
  --form "variables[JOB_ID]=$CI_JOB_ID" \
  --form "variables[REF]=$CI_COMMIT_REF_NAME" \
  --form "variables[SHA]=$CI_COMMIT_SHA" \
  https://gitlab.com/api/v4/projects/7388805/trigger/pipeline
```